<!--
SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# TEDective API
This directory is where TEDective source code "lives".
Apart from code itself, you may also found the script that are
used for API and database deployment.