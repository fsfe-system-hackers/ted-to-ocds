# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from setuptools import find_packages, setup

setup(
    name="TEDective API",
    version="0.1.0",
    packages=find_packages(),
    install_requires=[
        "fastapi>=0.89.0",
        "gunicorn>=20.1.0",
        "joblib>=1.1.0",
        "lxml>=4.9.1",
        "psycopg2-binary>=2.9.3",
        "python-dateutil>=2.8.2",
        "rich>=12.3.0",
        "uvicorn[standard]>=0.17.6",
        "orjson>=3.8.4",
        "asyncpg>=0.27.0",
        "pytest-asyncio>=0.20.3",
        "frozendict>=2.3.4",
        "types-lxml>=2022.11.8",
        "ftfy>=6.1.1",
        "fingerprints>=1.0.3",
        "phonenumbers>=8.13.5",
        "fastapi-versionizer>=0.1.2",
        "terminusdb-client[dataframe]>=10.2.3",
        "gql>=3.4.1",
        "aiohttp>=3.8.5",
    ],
    extras_require={"dev": ["pytest>=7.0"]},
    entry_points={
        "console_scripts": [
            "ingest=tedective_api.ingest:main",
            "database_init=tedective_api.database:init_db",
        ]
    },
)
