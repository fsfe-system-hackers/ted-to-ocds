# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>
# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

__version__ = "0.1.0"
