#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import sys
import tarfile
import time
from datetime import datetime
from glob import glob
from typing import Any, List
from urllib.request import urlretrieve

from dateutil import rrule
from joblib import Parallel, delayed
from lxml import etree
from rich import print as rprint
from rich.console import Console
from rich.progress import Progress

from tedective_api.database import get_connection
from tedective_api.ted_to_ocds.parser import ted_notice_to_ocds_releases
from tedective_api.utils import get_project_root, prune_list

# from terminusdb_client import Client


# Initialise rich console
console = Console()


def extract_ted(in_dir, year_month):
    """
    Extracts TED XMLs for a given year and month.

    :param in_dir: Directory where TED XML files are stored
    :param year_month: Year and month to extract
    """
    console.log(f"Extracting TED XMLs for {year_month}...")
    f_to_extract = tarfile.open(f"{in_dir}{year_month}.tar.gz")
    f_to_extract.extractall(f"{in_dir}{year_month}")
    os.remove(f"{in_dir}{year_month}.tar.gz")


def download_ted(in_dir: str, first_month: str, last_month: str) -> list:
    """
    Downloads and extracts TED data source data from https://ted.europa.eu in XML format.

    :param in_dir: Directory to store TED XML files
    :param first_month: First month to download, e.g. 2018-01
    :param last_month:  Last month to download, e.g. 2018-12
    :return: A list of directories which contain the TED XML files
    """
    downloaded = []
    start_date = datetime(
        int(first_month.split("-")[0]),
        int(first_month.split("-")[1]),
        1,
    )
    end_date = datetime(
        int(last_month.split("-")[0]),
        int(last_month.split("-")[1]),
        1,
    )
    # Iterate through the months
    for dt in rrule.rrule(rrule.MONTHLY, dtstart=start_date, until=end_date):
        year = dt.strftime("%Y")
        year_month = dt.strftime("%Y-%m")
        # Check whether downloaded and extracted
        if not os.path.isdir(f"{in_dir}{year_month}"):
            # Check whether downloaded
            if not os.path.isfile(f"{in_dir}{year_month}.tar.gz"):
                console.log(f"Downloading TED XMLs for {year_month}...")
                url = f"https://ted.europa.eu/xml-packages/monthly-packages/{year}/{year_month}.tar.gz"
                dst = f"{in_dir}{year_month}.tar.gz"
                urlretrieve(url, dst)
                extract_ted(in_dir, year_month)
                downloaded.append(f"{in_dir}{year_month}")
            # If already downloaded, only extract
            else:
                extract_ted(in_dir, year_month)
                downloaded.append(f"{in_dir}{year_month}")
        else:
            console.log(f"Already downloaded and extracted TED XML for {year_month}.")
    return downloaded


def get_ted_xml_files(in_dir: str, first_month: str, last_month: str) -> list:
    """
    Returns a sorted nested list of lists (one sublist per month) of TED XML files from
    a given directory

    :param in_dir: Directory where XML files are stored
    :param first_month: First month to get XML files for
    :param last_month: Last month to get XML files for
    :return: List of sublists of XML filepaths
    :rtype: list
    """
    xml_list = []
    start_date = datetime(
        int(first_month.split("-")[0]),
        int(first_month.split("-")[1]),
        1,
    )
    end_date = datetime(
        int(last_month.split("-")[0]),
        int(last_month.split("-")[1]),
        1,
    )

    for dt in rrule.rrule(rrule.DAILY, dtstart=start_date, until=end_date):
        sublist = []
        year_month = dt.strftime("%Y-%m")
        year_month_day = dt.strftime("%Y%m%d")
        for filename in glob(f"{in_dir}{year_month}/{year_month_day}*/*.xml"):
            sublist.append(filename)
        if len(sublist) >= 1:
            xml_list.append(sorted(sublist))
        else:
            continue

    return xml_list


def parse_ted_notice(xml_filepath: str) -> tuple:
    """Return a tuple containing the form type and the list of OCDS releases
    generated from it.

    :param xml_filepath: Path as string to the XML file of the TED notice
    :param ocid_prefix: String of the OCID prefix for your organisation
    :return: A tuple containing the form type and a list of OCDS releases
    :rtype: tuple
    """

    # Optimize performance for XMLParser
    parser = etree.XMLParser(ns_clean=True, huge_tree=True, remove_blank_text=True)

    # Define tree and generate cleaned root of XML
    tree: etree._ElementTree[Any] = etree.parse(xml_filepath, parser)  # type: ignore
    root: etree._Element = tree.getroot()

    # Define sections based on list indices
    sec_techn = root[0]
    sec_links = root[1]
    sec_coded: etree._Element = root[2]
    sec_trans = root[3]
    sec_forms = root[4]

    # This is the notice ID
    doc_id = sec_coded.find(
        ".//NOTICE_DATA/NO_DOC_OJS", namespaces=sec_coded.nsmap
    ).text
    # URI of current TED notice
    doc_id_short = root.get("DOC_ID")
    doc_uri = f"https://ted.europa.eu/udl?uri=TED:NOTICE:{doc_id_short}:TEXT:EN:HTML"

    # Define form type
    form_type = sec_forms[0].get("FORM")
    form_version = sec_forms[0].get("VERSION")
    if form_type is None:
        try:
            form_type = sec_forms[1].get("FORM")
            form_version = sec_forms[1].get("VERSION")
            if form_type is None:
                return form_version, None
        except IndexError:
            if form_version is None:
                return "invalid", None
            else:
                return form_version, None
    if form_type[0].isdigit():
        if len(form_type) == 1:
            form_type = f"F0{form_type}"
        else:
            form_type = f"F{form_type}"

    # Prune 'sec_forms' to only contain English version or if that is not available,
    # another language version. The language from which translation will be needed is
    # also passed
    translate_from = None
    for elem in sec_forms:
        if elem.tag.endswith("NOTICE_UUID"):
            continue
        lang: str = elem.get("LG")
        if lang == "EN":
            new_sec_forms: etree._Element = elem
            # Found an English form; stop iterating
            break
        # No English form was found. Translation will be needed
        new_sec_forms = elem
        translate_from = lang.lower()

    sec_forms = new_sec_forms

    # Check if 'OBJECT_CONTRACT' is indeed a list. If the list contains more than
    # one item, then multiple releases need to be generated.
    object_contract: List = sec_forms.findall(
        ".//OBJECT_CONTRACT", namespaces=sec_forms.nsmap
    )
    assert isinstance(object_contract, List)

    # Pass off to OCDS parser to generate releases
    releases = ted_notice_to_ocds_releases(
        doc_id=doc_id,
        form_type=form_type,
        ocid_prefix="ocds-jyvdv7-",
        object_contract=object_contract,
        doc_sec_forms=sec_forms,
        doc_sec_coded=sec_coded,
        doc_sec_trans=sec_trans,
        translate_from=translate_from,
    )
    return form_type, releases


def main(devel: bool):
    # Initialize connection and client
    client = get_connection()

    console.print("Downloading TED XML Data...")
    ted_folder = os.path.join("/in/ted")

    # TODO Make this configurable via CLI
    if devel:
        first_month = "2017-01"
        last_month = "2017-03"
    else:
        first_month = "2017-01"
        last_month = "2020-12"

    download_ted(ted_folder, first_month, last_month)
    xml_files = get_ted_xml_files(ted_folder, first_month, last_month)
    xml_files_total = len([i for sublist in xml_files for i in sublist])

    rprint()
    console.rule(f"Parsing {xml_files_total} TED Notices")
    rprint()

    total_notices = 0
    total_notices_unparsed = 0
    total_ocds_objects = 0

    with Parallel(n_jobs=-1) as parallel, Progress() as progress:
        task = progress.add_task("Parsing TED XML files...", total=xml_files_total)

        day_index = 0

        # Initialise set to store IDs of seen organizations
        seen_org_ids = set()

        while day_index < len(xml_files):
            day_size = len(xml_files[day_index])

            # Start timer to time parsing
            start_time_parsing = time.time()

            # Parse in parallel
            result = parallel(
                delayed(parse_ted_notice)(xml_file) for xml_file in xml_files[day_index]
            )

            if result is None:
                parsed_notices: List[tuple] = []
            else:
                parsed_notices: List[tuple] = result

            parsed_objects = [i[1] for i in parsed_notices if i[1] is not None]
            unparsed = [i for i in parsed_notices if i[1] is None or len(i[1]) == 0]

            # Flatten list of sublists of tuples into one list
            objects_to_insert = [obj for sublist in parsed_objects for obj in sublist]

            # Prune list OCDS objects to only contain unique items
            pruned = prune_list(objects_to_insert, seen_org_ids)
            pruned_list = pruned[0]
            seen_org_ids = pruned[1]

            # End timer to time parsing
            parsing_time = time.time() - start_time_parsing

            day_index += 1

            # Start timer to detect performance problems with database insertion
            start_time_inserting = time.time()

            client.insert_document(pruned_list)

            # End timer to time insertion
            insertion_time = round(time.time() - start_time_inserting, 2)

            total_notices += day_size
            total_notices_unparsed += len(unparsed)
            total_ocds_objects += len(objects_to_insert)

            # Print some information and advance progress bar
            progress.console.log(
                f"{day_index} day(s) and {total_notices} notices parsed. {total_ocds_objects} OCDS objects generated. "
                f"Parsing of {day_size} notices took {round(parsing_time, 2):4}s, insertion of {len(objects_to_insert)} OCDS objects took {insertion_time:4}s. "
                f"{len(unparsed):4} ({round((len(unparsed) / len(parsed_notices) * 100), 2)}%) notices weren't parsed. Overall coverage: {100 - round((total_notices_unparsed / total_notices) * 100, 2):5}%",
                highlight=False,
            )
            progress.advance(task, advance=day_size)


if __name__ == "__main__":
    devel = bool(sys.argv[1])
    main(devel)
