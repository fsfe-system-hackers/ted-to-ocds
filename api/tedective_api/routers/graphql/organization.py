# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import uuid

from fastapi import APIRouter, Depends, HTTPException

from tedective_api.routers.organization import read_organization

router = APIRouter()


@router.get("/graphql/organization/{id}", tags=["awards"])
async def get_organization(id: uuid.UUID):
    response = await read_organization(id)
    graph = convert_response_to_graph(response)
    return graph


def convert_response_to_graph(organization_data):
    nodes = []
    links = []

    org_node = {""}
