# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import uuid

from fastapi import APIRouter, Depends, FastAPI, HTTPException, Query
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport
from gql.transport.exceptions import TransportProtocolError, TransportServerError

router = APIRouter()


@router.get(
    "/organization/{id}",
    tags=["organization"],
    description="Read organization by ID",
)
async def read_organization(id: uuid.UUID):
    """Return organization by ID

    :param id:
    """
    address = os.environ["ADDRESS_GQL"]
    headers = os.environ["HEADERS"]

    transport = AIOHTTPTransport(url=address, headers={"Authorization": str(headers)})
    async with Client(
        transport=transport, fetch_schema_from_transport=True, execute_timeout=1000000
    ) as client:
        query = gql(
            """
            query getOrganization($id: String!) {
                Organization (filter: {id: {eq: $id}}) {
                    id
                    name
                        identifier {
                            scheme
                            legalName
                            uri
                        }
                        additionalIdentifiers {
                            scheme
                            legalName
                            uri
                        }
                        address {
                            country {
                                name
                                also_know_as
                            }
                            postal_code
                            street
                        }
                        contactPoint {
                            name
                            email
                            telephone
                            faxNumber
                            url
                        }
                        roles
                        details 
                    }
                }
            """
        )
        params = {"id": str(id)}
        try:
            result = await client.execute(query, variable_values=params)
            return result["Organization"][0]
        except IndexError or TransportProtocolError or TransportServerError:
            raise HTTPException(status_code=404, detail="Organization not found")


# @router.get(
#     "/organization/search",
#     response_model=OrganizationRead,
#     tags=["organization"],
#     description="Read organization by ID",
# )
# async def search_organization(*, session: Session = Depends(get_session), query: str):
#     """
#
#     :param session:
#     :param query:
#     """
#     pass
