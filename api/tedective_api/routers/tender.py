# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import uuid

from fastapi import APIRouter, Depends, HTTPException, Query
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport
from gql.transport.exceptions import TransportProtocolError, TransportServerError

router = APIRouter()


@router.get(
    "/tender/{id}",
    tags=["tenders"],
)
async def read_tender(id: str):
    address = os.environ["ADDRESS_GQL"]
    headers = os.environ["HEADERS"]

    transport = AIOHTTPTransport(url=address, headers={"Authorization": str(headers)})
    async with Client(
        transport=transport, fetch_schema_from_transport=True, execute_timeout=1000000
    ) as client:
        query = gql(
            """
            query getTender($id: String!) {
                Tender (filter: {id: {eq: $id}}) {
                    id
                    title
                    description
                    status
                    procuringEntity {
                        name
                        identifier {
                            scheme
                            legalName
                            uri
                        }
                        additionalIdentifiers {
                            scheme
                            legalName
                            uri
                        }
                        address {
                            country {
                                name
                                also_know_as
                            }
                            postal_code
                            street
                        }
                        contactPoint {
                            name
                            email
                            telephone
                            faxNumber
                            url
                        }
                        roles
                        details  
                    }
                    items {
                        description
                        classification {
                            scheme
                            description
                            uri   
                        }
                        additionalClassifications {
                            scheme
                            description
                            uri 
                        }
                        quantity
                        unit {
                            scheme
                            name
                            value {
                                amount
                                currency
                            }
                            uri 
                        }
                    }
                    value {
                        amount
                        currency
                    }
                    minValue {
                        amount
                        currency
                    }
                    procurementMethod 
                    procurementMethodDetails
                    procurementMethodRationale
                    mainProcurementCategory
                    additionalProcurementCategories
                    awardCriteria
                    awardCriteriaDetails
                    submissionMethod
                    submissionMethodDetails
                    tenderPeriod {
                        startDate
                        endDate
                        maxExtentDate 
                        durationInDays  
                    }
                    enquiryPeriod {
                        startDate
                        endDate
                        maxExtentDate 
                        durationInDays 
                    }
                    hasEnquiries
                    eligibilityCriteria
                    awardPeriod {
                        startDate
                        endDate
                        maxExtentDate 
                        durationInDays
                    }
                    contractPeriod {
                        startDate
                        endDate
                        maxExtentDate 
                        durationInDays
                    }
                    numberOfTenderers
                    tenderers {
                        name
                        identifier {
                            scheme
                            legalName
                            uri
                        }
                        additionalIdentifiers {
                            scheme
                            legalName
                            uri
                        }
                        address {
                            country {
                                name
                                also_know_as
                            }
                            postal_code
                            street
                        }
                        contactPoint {
                            name
                            email
                            telephone
                            faxNumber
                            url
                        }
                        roles
                        details 
                    }
                    documents {
                        documentType
                        title
                        description
                        url
                        datePublished
                        dateModified
                        format
                        language
                    }
                    milestones {
                        title
                        type
                        description
                        code
                        dueDate
                        dateMet
                        dateModified
                        status
                        documents {
                            documentType
                            title
                            description
                            url
                            datePublished
                            dateModified
                            format
                            language
                        } 
                    }
                    amendments {
                        date
                        rationale
                        description
                        amendsReleaseID
                        releaseID
                        changes {
                            property
                            former_value
                        }   
                    }
                    amendment {
                        date
                        rationale
                        description
                        amendsReleaseID
                        releaseID
                        changes {
                            property
                            former_value
                        } 
                    }
                    crossBorderLaw 
                }
            }
            """
        )
        params = {"id": id}
        try:
            result = await client.execute(query, variable_values=params)
            return result["Tender"][0]
        except IndexError or TransportProtocolError or TransportServerError:
            raise HTTPException(status_code=404, detail="Tender not found")


# @router.get(
# "/tenders",
# response_model=List[TenderRead],
# tags=["tenders"],
# response_model_exclude_none=True,
# )
# async def read_tender(
# *,
# session: Session = Depends(get_session),
# procuring_entity_id: uuid.UUID = None,
# offset: int = 0,
# limit: int = Query(default=100, lte=100),
# ):
# # NOTE Until now, this is not set.
# if procuring_entity_id is not None:
#     result = await session.execute(
#         select(Tender)
#         .where(Tender.procuringEntity_id == procuring_entity_id)
#         .offset(offset)
#         .limit(limit)
#     )
#
# else:
#     result = await session.execute(
#         select(Tender).where(Tender.id == ocid).offset(offset).limit(limit)
#     )
#
# try:
#     return result.scalars().all()
# except sqlalchemy.exc.NoResultFound:
#     raise HTTPException(status_code=404, detail="Tender not found")
#
#
# @router.get(
# "/tenders/{ocid}",
# response_model=List[TenderRead],
# tags=["tenders"],
#
# )
# async def read_tender_ocid(
# *,
# session: Session = Depends(get_session),
# ocid: str,
# offset: int = 0,
# limit: int = Query(default=100, lte=100),
# ):
# result = await session.execute(
#     select(Tender).where(Tender.id == ocid).offset(offset).limit(limit)
# )
# result = result.scalars().all()
# if not result:
#     raise HTTPException(status_code=404, detail="No tenders found for that OCID.")
# return result
