# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os

import orjson
from forex_python.converter import CurrencyRates

from tedective_api.utils import get_project_root


def update_exchange_rates():
    c = CurrencyRates()
    rates = c.get_rates("EUR")
    with open(os.path.join(get_project_root(), "in/currency/rates.json"), "wb") as file:
        file.write(orjson.dumps(rates, option=orjson.OPT_INDENT_2))


if __name__ == "__main__":
    update_exchange_rates()
