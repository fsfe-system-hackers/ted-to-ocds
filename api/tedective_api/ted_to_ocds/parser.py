# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import uuid
from datetime import datetime
from typing import List, Optional, Set

import fingerprints
from lxml import etree

from tedective_api import schema

from . import extractors, trackers


def ted_notice_to_ocds_releases(
    doc_id: str,
    form_type: str,
    ocid_prefix: str,
    object_contract: List[etree._Element],
    doc_sec_coded: etree._Element,
    doc_sec_forms: etree._Element,
    doc_sec_trans: etree._Element,
    translate_from: Optional[str] = None,
) -> list:
    """
    Returns a list of OCDS releases from the XML source of TED notices.
    See https://standard.open-contracting.org/profiles/eu/latest/en/forms/F01/
    for more information.

    :param form_type:
    :param ocid_prefix:
    :param doc_id:
    :param object_contract:
    :param doc_sec_forms:
    :param doc_sec_coded:
    :param doc_sec_trans:
    :param translate_from:
    :rtype:
    """
    ocds_objects = []

    # Only process implemented form types
    implemented_forms = ["F01", "F02", "F03"]
    if form_type not in implemented_forms:
        return []

    # A simple counter to keep track of the number of elements in one '/OBJECT_CONTRACT'
    elem_count = 0

    # Generate an OCDS release for each '/OBJECT_CONTRACT' element
    elem: etree._Element
    for elem in object_contract:
        elem_count += 1
        # Define Release object instance and set some initial values
        release = schema.Release(
            # The OCID will be set later
            ocid="",
            language="en",
            date=datetime.strptime(
                doc_sec_coded.find(".//DATE_PUB", namespaces=doc_sec_coded.nsmap).text,
                "%Y%m%d",  # type: ignore
            ),
            initiationType=schema.InitiationType.tender,
        )

        # Define linked objects and optionally set some defaults
        tender_uuid = uuid.uuid4()
        tender = schema.Tender(
            uid=tender_uuid, legalBasis=schema.Classification(scheme="CELEX")
        )

        release.tender = tender
        # F01
        if form_type == "F01":
            # https://standard.open-contracting.org/profiles/eu/latest/en/forms/F01/#preamble
            notice_type = doc_sec_forms.find(  # type: ignore
                ".//NOTICE", namespaces=doc_sec_forms.nsmap
            ).get("TYPE")
            if notice_type == "PRI_ONLY" or notice_type == "PRI_REDUCING_TIME_LIMITS":
                release.tag = [schema.Tag.planning]
                tender.status = schema.Status.planned
            if notice_type == "PRI_CALL_COMPETITION":
                release.tag = [schema.Tag.planning, schema.Tag.tender]
                tender.status = schema.Status.active

        # F02
        elif form_type == "F02":
            release.tag = [schema.Tag.tender]
            tender.status = schema.Status.active

        # F03
        elif form_type == "F03":
            release.tag = [schema.Tag.award, schema.Tag.contract]
            tender.status = schema.Status.complete

        # Section I - Contracting Authority (CA)
        # https://standard.open-contracting.org/profiles/eu/latest/en/forms/F01/#section-i
        # I.1 Name and addresses
        buyer_name = extractors.extract_text(
            doc_sec_forms,
            ".//CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/OFFICIALNAME",
        )

        buyer_identifier = schema.Identifier(legalName=buyer_name)
        buyer_identifier.id = extractors.extract_text(
            doc_sec_forms,
            ".//CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/NATIONALID",
        )
        buyer_identifier.scheme = "National-ID"

        buyer_address = extractors.extract_buyer_address(doc_sec_forms)
        buyer_contactPoint = extractors.extract_buyer_contact_point(doc_sec_forms)
        buyer_details = extractors.extract_buyer_details(doc_sec_forms)
        buyer_details_list = set(buyer_details)

        # Generate a heavily-scrubbed fingerprint from the organization's name
        buyer_fingerprint = fingerprints.generate(buyer_name)
        # Generate UUID based on that fingerprint. Same fingerprint, same UUID.
        buyer_uuid = uuid.uuid5(uuid.NAMESPACE_URL, buyer_fingerprint)
        buyer = schema.Organization(
            id=str(buyer_uuid),
            fingerprint=buyer_fingerprint,
            name=buyer_name,
            identifier=buyer_identifier,
            address=buyer_address,
            contactPoint=buyer_contactPoint,
            details=buyer_details_list,
            roles={"buyer"},
        )
        ocds_objects.append(buyer)
        ocds_objects.append(tender)
        ocds_objects.append(release)

        release.buyer = set()
        release.buyer.add(buyer)
        # ocds_objects.append(
        #     schema.OrganizationReleaseLink(
        #         release_id=release.id, organization_id=buyer.id
        #     )
        # )

        additional_buyer = doc_sec_forms.find(
            ".//CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY_ADDITIONAL",
            namespaces=doc_sec_forms.nsmap,
        )
        if additional_buyer is not None:
            add_buyer_name = extractors.extract_text(
                doc_sec_forms,
                ".//CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY_ADDITIONAL/OFFICIALNAME",
            )

            add_buyer_identifier = schema.Identifier(legalName=add_buyer_name)
            add_buyer_identifier.id = extractors.extract_text(
                doc_sec_forms,
                ".//CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY_ADDITIONAL/NATIONALID",
            )
            add_buyer_identifier.scheme = "National-ID"

            add_buyer_address = extractors.extract_buyer_address(
                doc_sec_forms, "ADDRESS_CONTRACTING_BODY_ADDITIONAL"
            )
            add_buyer_contactPoint = extractors.extract_buyer_contact_point(
                doc_sec_forms, "ADDRESS_CONTRACTING_BODY_ADDITIONAL"
            )
            add_buyer_details = extractors.extract_buyer_details(
                doc_sec_forms, "ADDRESS_CONTRACTING_BODY_ADDITIONAL"
            )
            # Generate a heavily-scrubbed fingerprint from the organization's name
            add_buyer_fingerprint = fingerprints.generate(add_buyer_name)
            # Generate UUID based on that fingerprint. Same fingerprint, same UUID.
            add_buyer_uuid = uuid.uuid5(uuid.NAMESPACE_URL, add_buyer_fingerprint)
            add_buyer = schema.Organization(
                id=str(add_buyer_uuid),
                fingerprint=add_buyer_fingerprint,
                name=add_buyer_name,
                identifier=add_buyer_identifier,
                address=add_buyer_address,
                contactPoint=add_buyer_contactPoint,
                details=set(add_buyer_details),
                roles={"buyer"},
            )
            if add_buyer_uuid == buyer_uuid:
                # Additional buyer is the same as primary buyer, do nothing
                pass

            else:
                # Additional buyer is new, add to tracker and database
                ocds_objects.append(add_buyer)
                release.buyer.add(add_buyer)
                # ocds_objects.append(
                #     schema.OrganizationReleaseLink(
                #         release_id=release.id, organization_id=add_buyer.id
                #     )
                # )

        # I.2 Information about joint procurement
        cb_pl = doc_sec_forms.find(
            ".//CONTRACTING_BODY/PROCUREMENT_LAW", namespaces=doc_sec_forms.nsmap
        )
        if cb_pl is not None:
            tender.crossBorderLaw = cb_pl[0].text
        cp = doc_sec_forms.find(
            ".//CONTRACTING_BODY/CENTRAL_PURCHASING", namespaces=doc_sec_forms.nsmap
        )
        # if cp is not None:
        #     buyer.roles.append("centralPurchasingBody")

        # TODO I.3 Communication
        cb_url_p = doc_sec_forms.find(
            ".//CONTRACTING_BODY/URL_PARTICIPATION", namespaces=doc_sec_forms.nsmap
        )
        if cb_url_p is not None:
            # tender.submissionMethod = ["electronicSubmission"]
            tender.submissionMethodDetails = cb_url_p.text

        classifications = []
        # I.4 Type of the contracting authority
        buyer_details_classification = schema.Classification(
            scheme="TED_CA_TYPE",
            id=str(
                extractors.extract_attribute(
                    doc_sec_forms, ".//CONTRACTING_BODY/CA_TYPE", "VALUE"
                )
            ),
        )
        if buyer_details_classification.id is not None:
            classifications.append(buyer_details_classification)

        # I.5 Main activity
        buyer_details_main_activity = schema.Classification(
            scheme="COFOG",
            id=extractors.extract_attribute(
                doc_sec_forms, ".//CONTRACTING_BODY/CA_ACTIVITY", "VALUE"
            ),
        )
        if buyer_details_main_activity.id is not None:
            classifications.append(buyer_details_main_activity)

        # TODO: check if this workaround does work
        if len(classifications) > 0:
            buyer.details.add(str({"classifications": classifications}.items()))

        # Section II
        # https://standard.open-contracting.org/profiles/eu/latest/en/forms/F01/#section-ii
        # II.1 Scope of the procurement
        # II.1.1 Title
        tender.title = extractors.extract_title(elem, doc_sec_trans)

        # II.1.2 Main CPV Code
        classification = schema.Classification(
            scheme="CPV", id=extractors.extract_attribute(elem, ".//CPV_CODE", "CODE")
        )
        # tender.classification = classification

        # TODO II.1.3
        # II.1.4 Short description
        tender.description = extractors.extract_description(elem)

        # II.1.5 Estimated Total Value
        tender.value = extractors.extract_value(doc_sec_forms, "tender")

        # Section V - Award of contract
        if form_type == "F03":
            # Check if there are more awards
            award_elems = doc_sec_forms.findall(
                ".//AWARD_CONTRACT", namespaces=doc_sec_forms.nsmap
            )
            award_elem: etree._Element
            for award_elem in award_elems:
                award_id = uuid.uuid4()
                award_title = extractors.extract_title(award_elem, doc_sec_trans)
                award = schema.Award(id=str(award_id), title=award_title)

                if extractors.extract_text(award_elem, ".//AWARDED_CONTRACT"):
                    award.status = schema.AwardStatus.active
                    contract = schema.Contract(
                        id=str(uuid.uuid4()),
                        awardID=str(award_id),
                        title=award_title,
                        description=None,
                        status=schema.ContractStatus("active"),
                        period=None,
                        value=extractors.extract_value(award_elem, "award"),
                        items=None,
                        dateSigned=extractors.extract_text(
                            award_elem,
                            ".//DATE_CONCLUSION_CONTRACT",
                        ),
                        documents=None,
                    )
                    release.contracts = set()
                    ocds_objects.append(contract)
                    release.contracts.add(contract)
                    # ocds_objects.append(
                    #     schema.ContractReleaseLink(
                    #         contract_id=contract.id, release_id=release.id
                    #     )
                    # )
                else:
                    award.status = schema.AwardStatus.unsuccessful

                # V.2.3 Name and address of the contractor(s)
                supplier_elems = award_elem.findall(
                    ".//CONTRACTOR",
                    namespaces=award_elem.nsmap,
                )

                ocds_objects.append(award)

                for supplier_elem in supplier_elems:
                    supplier_name = extractors.extract_text(
                        supplier_elem, ".//OFFICIALNAME"
                    )
                    supplier_fingerprint = fingerprints.generate(supplier_name)
                    supplier_id = uuid.uuid5(uuid.NAMESPACE_URL, supplier_fingerprint)
                    supplier = schema.Organization(
                        id=str(supplier_id),
                        name=supplier_name,
                        fingerprint=supplier_fingerprint,
                        identifier=schema.Identifier(
                            id=extractors.extract_text(supplier_elem, ".//NATIONALID"),
                            scheme="National-ID",
                            legalName=supplier_name,
                        ),
                        roles={"supplier"},
                        address=extractors.extract_supplier_address(supplier_elem),
                    )

                    award.suppliers = set()
                    award.suppliers.add(supplier)

                    # link = schema.OrganizationAwardLink(
                    #     organization_id=supplier.id, award_id=award.id
                    # )

                    # This is a new supplier
                    ocds_objects.append(supplier)

                    # if link not in ocds_objects:
                    #     ocds_objects.append(link)
                    # iterated through all suppliers for award

                # iterated through all awards

            # Link suppliers to release
            # suppliers = [
            #     party
            #     for party in ocds_objects
            #     if type(party) == schema.Organization and "supplier" in party.roles
            # ]
            # release.parties = set()
            # for supplier in suppliers:
            #     release.parties.add(supplier)
            #     ocds_objects.append(
            #         schema.OrganizationReleaseLink(
            #             organization_id=supplier.id, release_id=release.id
            #         )
            #     )

            if tender not in ocds_objects:
                ocds_objects.append(tender)
            if release not in ocds_objects:
                ocds_objects.append(release)

            release.awards = set()
            release.awards.add(award)
            # ocds_objects.append(
            #     schema.AwardReleaseLink(release_id=release.id, award_id=award.id)
            # )

        # Generate OCID
        if form_type == "F01" and len(object_contract) == 1:
            # If it's an F01 form and there is only one '/OBJECT_CONTRACT' element,
            # generate an OCID based on 'DOC_ID'
            release.ocid = ocid_prefix + str(uuid.uuid5(uuid.NAMESPACE_URL, doc_id))
        elif form_type == "F01" and len(object_contract) > 1:
            # There are more than one '/OBJECT_CONRACT' elements, for each of which a
            # new release with a new OCID needs to be generated, so use the counter
            release.ocid = ocid_prefix + str(
                uuid.uuid5(uuid.NAMESPACE_URL, f"{doc_id}-{elem_count}")
            )
        elif form_type == "F02" or form_type == "F03":
            related_doc_id = extractors.extract_text(
                doc_sec_forms, ".//PROCEDURE/NOTICE_NUMBER_OJ"
            )
            if related_doc_id is not None:
                trackers.related_doc_id_count += 1

            if related_doc_id is not None:
                if len(object_contract) > 1:
                    release.ocid = ocid_prefix + str(
                        uuid.uuid5(uuid.NAMESPACE_URL, f"{related_doc_id}-{elem_count}")
                    )
                else:
                    release.ocid = ocid_prefix + str(
                        uuid.uuid5(uuid.NAMESPACE_URL, related_doc_id)
                    )
            else:
                if len(object_contract) > 1:
                    release.ocid = ocid_prefix + str(
                        uuid.uuid5(uuid.NAMESPACE_URL, f"{doc_id}-{elem_count}")
                    )
                else:
                    release.ocid = ocid_prefix + str(
                        uuid.uuid5(uuid.NAMESPACE_URL, doc_id)
                    )
        else:
            release.ocid = ocid_prefix

        # Set tender.id to OCID of the release
        tender.id = release.ocid

        # Prepare insertion into the database in ../ingest.py
        # TODO: commented out for now check the purpose of it
        # release.tender_id = tender_uuid
        # 'release.buyer_id' always refers to the primary buyer
        # release.buyer_id = buyer_uuid
        # Insert the rest of the object if they aren't already
        if tender not in ocds_objects:
            ocds_objects.append(tender)
        if release not in ocds_objects:
            ocds_objects.append(release)

        # End of for-loop

    # After having iterated through all '/OBJECT_CONTRACT' elements, return releases
    return ocds_objects
