# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import uuid
from typing import List

# List to store seen parties
seen_parties: List[uuid.UUID] = []  # type: ignore

related_doc_id_count = 0
