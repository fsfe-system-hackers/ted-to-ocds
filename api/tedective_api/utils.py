# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import re
from datetime import date, datetime
from functools import cache
from pathlib import Path
from typing import Optional

import ftfy
import orjson
from _decimal import Decimal

from tedective_api import schema


def get_project_root() -> Path:
    return Path(__file__).parent.parent


def ocds_json_serializer(obj):
    """Custom JSON serializer for database insertion"""
    if isinstance(
        obj,
        (
            schema.TagEnum,
            schema.InitiationType,
            schema.Status,
            schema.ProcurementMethod,
            schema.MainProcurementCategory,
            schema.AwardStatus,
            schema.ContractStatus,
            schema.MilestoneStatus,
            schema.Currency,
        ),
    ):
        return str(obj)
    # elif issubclass(type(obj), .SQLModel):
    # This allows quick and dirty insertion of JSON into models
    #    return obj.__dict__
    if isinstance(obj, set):
        return list(obj)
    if isinstance(obj, (datetime, date)):
        return obj.isoformat()


@cache
def clean(string: str) -> str:
    try:
        string = re.sub(r"\s+", " ", string)
        string = string.strip()
        string = ftfy.fix_text(string)
        return string
    except TypeError:
        return None


@cache
def convert_to_euro(amount: str, currency: str) -> Optional[Decimal]:
    with open(os.path.join("/app/in/rates.json"), "rb") as f:
        rates = orjson.loads(f.read())  # type: ignore
    try:
        return Decimal(round(float(amount) / float(rates[currency]), 2))
    except KeyError:
        return None


def prune_list(list_of_objects, seen_ids):
    filtered_list = []
    ids = seen_ids
    for obj in list_of_objects:
        if type(obj) == schema.Organization and obj.id not in ids:
            filtered_list.append(obj)
            ids.add(obj.id)
        elif type(obj) == schema.Organization and obj.id in ids:
            continue
        else:
            filtered_list.append(obj)

    return list(set(filtered_list)), seen_ids
