# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: CC0-1.0

# Use the official Node.js LTS image as the base
FROM node:lts

# Set the working directory
WORKDIR /app

# Copy package.json and package-lock.json (or yarn.lock) to cache dependencies
COPY package*.json ./

# Install dependencies
RUN npm ci --only=production

# Copy the rest of the app files
COPY . ./

# Build the Next.js app for production
RUN npm run build

# Expose the port for the application
EXPOSE 3000

# Start the app
CMD ["npm", "start"]
