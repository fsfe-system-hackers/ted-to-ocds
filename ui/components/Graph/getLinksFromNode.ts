// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { TEDectiveNode, LinksByOcid, OcidsByOrgId } from '@/pages'

export default function getLinksFromNode({
  node,
  linksByOcid,
  ocidsByOrgId,
}: {
  node: TEDectiveNode
  linksByOcid: LinksByOcid
  ocidsByOrgId: OcidsByOrgId
}) {
  switch (node.type) {
    case 'organization':
      const orgId = node.data.id
      const ocids = Array.from(ocidsByOrgId[orgId] ?? [])
      return ocids.flatMap((ocid) => linksByOcid[ocid])
    case 'release':
      return linksByOcid[node.data.ocid]
  }
}
