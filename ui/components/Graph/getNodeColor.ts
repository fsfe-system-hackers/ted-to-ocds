// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { initialVisuals } from '@/components/config'
import { TEDectiveNode } from '@/pages'
import { getOpacity } from './getOpacity'
import { getThemeColor } from './getThemeColor'
import hexToRGBA from './hexToRGBA'

const getColor = (node: TEDectiveNode, visuals: typeof initialVisuals) => {
  switch (node.type) {
    case 'organization':
      return visuals.orgNodeColor
    case 'release':
      if (node.data.tag.includes('award')) {
        return visuals.awardNodeColor
      } else if (node.data.tag.includes('tender')) {
        return visuals.tenderNodeColor
      } else {
        return visuals.planningNodeColor
      }
  }
}

export default function getNodeColor({
  node,
  theme,
  visuals,
  opacity,
  hoverNode,
  highlightedNodes,
}: {
  node: TEDectiveNode
  theme: any
  visuals: typeof initialVisuals
  opacity: number
  hoverNode: TEDectiveNode | null
  highlightedNodes: Set<TEDectiveNode>
}) {
  const needsHighlighting = highlightedNodes.has(node)

  const color = getColor(node, visuals)

  const nodeColor = getThemeColor(color, theme)

  const nodeOpacity = getOpacity(visuals, hoverNode, opacity, needsHighlighting)

  return hexToRGBA(nodeColor, nodeOpacity)
}
