// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { TEDectiveNode } from '@/pages'
import { initialVisuals } from '../config'

export default function getNodeSize({
  node,
  highlightedNodes,
  opacity,
  visuals,
}: {
  node: TEDectiveNode
  highlightedNodes: Set<TEDectiveNode>
  opacity: number
  visuals: typeof initialVisuals
}) {
  let basicSize = 3
  switch (node.type) {
    case 'organization':
      basicSize += visuals.orgNodeSize - 1
      break
    case 'release':
      basicSize +=
        (node.data.contractsValue() / 100000) * visuals.awardNodeSizeMult
      break
  }
  // TODO sometimes `highlightedNodes === undefined`?
  const highlightSize = highlightedNodes?.has(node)
    ? 1 + opacity * (visuals.highlightNodeSize - 1)
    : 1
  return basicSize * highlightSize
}
