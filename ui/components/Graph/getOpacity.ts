// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later
import { TEDectiveNode } from '@/pages'
import { initialVisuals } from '../config'

export const getOpacity = (
  visuals: typeof initialVisuals,
  hoverNode: TEDectiveNode | null,
  opacity: number,
  needsHighlighting: boolean
) => {
  return !hoverNode || needsHighlighting
    ? opacity
    : -1 * (visuals.highlightFade * opacity - 1)
}
