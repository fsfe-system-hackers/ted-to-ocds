// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

export const getThemeColor = (name: string, theme: any) => {
  return name.split('.').reduce((o, i) => o[i], theme.colors)
}
