// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

// taken from https://github.com/org-roam/org-roam-ui/blob/main/util/hexToRGBA.ts

export default function hexToRGBA(hex: string, opacity: number) {
  return (
    'rgba(' +
    (hex = hex.replace('#', ''))
      .match(new RegExp('(.{' + hex.length / 3 + '})', 'g'))!
      .map((l) => parseInt(hex.length % 2 ? l + l : l, 16))
      .concat(isFinite(opacity) ? opacity : 1)
      .join(',') +
    ')'
  )
}
