// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { forceX, forceY } from 'd3-force-3d'
import { useWindowSize } from '@react-hook/window-size'
import { GraphData, NodeObject } from 'force-graph'
import {
  ComponentPropsWithoutRef,
  useEffect,
  useRef,
  useState,
  useMemo,
} from 'react'
import { ForceGraph2D } from 'react-force-graph'
import getNodeColor from './getNodeColor'
import getNodeSize from './getNodeSize'
import getLinksFromNode from './getLinksFromNode'
import { TEDectiveLink, TEDectiveNode } from '@/pages'
import getLinkColor from './getLinkColor'
import { initialPhysics, initialVisuals } from '../config'
import drawLabel from './drawLabel'
import { isLinkRelatedToNode } from './isLinkRelatedToNode'
import { LinksByOcid, OcidsByOrgId } from '@/pages'
import { useTheme } from '@chakra-ui/react'

export interface GraphProps {
  graphData: GraphData
  linksByOcid: LinksByOcid
  ocidsByOrgId: OcidsByOrgId
  setPreviewNode: any
  physics: typeof initialPhysics
  visuals: typeof initialVisuals
}

export default function Graph({
  graphData,
  linksByOcid,
  ocidsByOrgId,
  setPreviewNode,
  physics,
  visuals,
}: GraphProps) {
  const [hoverNode, setHoverNode] = useState<TEDectiveNode | null>(null)
  const [opacity, setOpacity] = useState(1)
  const [windowWidth, windowHeight] = useWindowSize()
  const theme = useTheme()
  const graphRef = useRef<any>(null)

  useEffect(() => {
    const fg = graphRef.current
    if (physics.gravityOn) {
      fg.d3Force('x', forceX().strength(physics.gravity))
      fg.d3Force('y', forceY().strength(physics.gravity))
    } else {
      fg.d3Force('x', null)
      fg.d3Force('y', null)
    }
    //  physics.centering
    //      ? fg.d3Force('center', forceCenter().strength(physics.centeringStrength))
    //      : fg.d3Force('center', null)
    physics.linkStrength && fg.d3Force('link').strength(physics.linkStrength)
    // physics.linkIts && fg.d3Force('link').iterations(physics.linkIts)
    physics.charge && fg.d3Force('charge').strength(physics.charge)
  }, [physics])

  // Normally the graph doesn't update when you just change the physics parameters
  // This forces the graph to make a small update when you do
  useEffect(() => {
    graphRef.current?.d3ReheatSimulation()
  }, [physics])

  const highlightedNodes = useMemo(() => {
    if (!hoverNode) {
      return new Set<TEDectiveNode>()
    }

    const links = getLinksFromNode({
      node: hoverNode,
      linksByOcid,
      ocidsByOrgId,
    })

    if (!links) {
      return new Set<TEDectiveNode>()
    }
    return new Set(links.flatMap((link) => [link.source, link.target]))
  }, [hoverNode, linksByOcid, ocidsByOrgId])

  useEffect(() => {
    if (!visuals.highlightAnim) {
      return hoverNode ? setOpacity(1) : setOpacity(0)
    }
  }, [hoverNode, visuals])

  const graphCommonProps: ComponentPropsWithoutRef<typeof ForceGraph2D> = {
    graphData: graphData!,
    width: windowWidth,
    height: windowHeight,
    nodeLabel: () => '',
    nodeRelSize: visuals.nodeRel,
    nodeVal: (node) =>
      getNodeSize({
        node: node as TEDectiveNode,
        highlightedNodes,
        opacity,
        visuals,
      }),
    nodeColor: (node) =>
      getNodeColor({
        node: node as TEDectiveNode,
        theme,
        visuals,
        opacity,
        hoverNode,
        highlightedNodes,
      }),
    nodeCanvasObjectMode: () => 'after',
    nodeCanvasObject: (node, ctx, globalScale) => {
      drawLabel({
        node: node as TEDectiveNode,
        theme,
        hoverNode,
        highlightedNodes,
        ctx,
        globalScale,
        opacity,
        visuals,
      })
    },
    linkColor: (link) => {
      return getLinkColor({
        link: link as TEDectiveLink,
        theme,
        visuals,
        hoverNode,
        opacity,
        highlightedNodes,
      })
    },
    linkWidth: (linkArg) => {
      const link = linkArg as TEDectiveLink
      if (visuals.highlightLinkSize === 1) {
        return visuals.linkWidth
      }
      const linkIsHighlighted = isLinkRelatedToNode(link, highlightedNodes)
      return linkIsHighlighted
        ? visuals.linkWidth * (1 + opacity * (visuals.highlightLinkSize - 1))
        : visuals.linkWidth
    },
    onNodeClick: (node: NodeObject, event: any) => {
      setPreviewNode(node as TEDectiveNode)
    },
    onNodeHover: (node) => {
      setHoverNode(node as TEDectiveNode)
    },
    onNodeDrag: (node) => {
      setHoverNode(node as TEDectiveNode)
    },
    onNodeDragEnd: () => {
      setHoverNode(null)
    },
    d3AlphaDecay: physics.alphaDecay,
    d3AlphaMin: physics.alphaMin,
    d3VelocityDecay: physics.velocityDecay,
  }

  return <ForceGraph2D ref={graphRef} {...graphCommonProps} />
}
