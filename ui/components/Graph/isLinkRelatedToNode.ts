// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { TEDectiveLink, TEDectiveNode } from '@/pages'

export const isLinkRelatedToNode = (
  link: TEDectiveLink,
  highlightedNodes: Set<TEDectiveNode>
) => {
  return highlightedNodes.has(link.source) && highlightedNodes.has(link.target)
}
