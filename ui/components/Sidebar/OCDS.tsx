// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { TEDectiveNode } from '../../pages/index'
import { OrganizationCard } from './OrganizationCard'
import { ReleaseCard } from './ReleaseCard'

export interface OCDSProps {
  previewNode: TEDectiveNode
}

export const OCDS = ({ previewNode }: OCDSProps) => {
  switch (previewNode.type) {
    case 'organization':
      return <OrganizationCard org={previewNode.data} />
    case 'release':
      return <ReleaseCard release={previewNode.data} />
  }
}
