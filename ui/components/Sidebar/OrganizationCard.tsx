// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import {
  Card,
  CardBody,
  CardHeader,
  Heading,
  Box,
  Text,
  Stack,
  StackDivider,
} from '@chakra-ui/react'

import { Organization } from '../../models/Organization'

export interface OrganizationProps {
  org: Organization
}

export const OrganizationCard = ({ org }: OrganizationProps) => {
  console.log(JSON.stringify(org, undefined, 2))
  return (
    <Card>
      <CardHeader>
        <Heading size="md">{org.name}</Heading>
      </CardHeader>
      <CardBody>
        <Stack divider={<StackDivider />} spacing="4">
          <Box>
            <Heading size="xs">Address</Heading>
            <Stack spacing={0} mt={2}>
              {org.formatAddress().map((s, i) => {
                return (
                  <Text key={i} fontSize="sm">
                    {s}
                  </Text>
                )
              })}
            </Stack>
          </Box>
          {org.contactPoint && (
            <Box>
              <Heading size="xs">Contact</Heading>
              <Stack spacing="0" mt={2}>
                {org.formatContactPoint().map((s, i) => {
                  return (
                    <Text key={i} fontSize="sm">
                      {s}
                    </Text>
                  )
                })}
              </Stack>
            </Box>
          )}
        </Stack>
      </CardBody>
    </Card>
  )
}
