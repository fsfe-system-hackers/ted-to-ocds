// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Card,
  CardBody,
  CardHeader,
  Divider,
  Heading,
  SimpleGrid,
  Stack,
  Text,
} from '@chakra-ui/react'
import assert from 'assert'
import { components } from '../../api'
import { Release } from '../../models/Release'
import { OrganizationCard } from './OrganizationCard'

type Contract = components['schemas']['Contract']

const sumContractsValue = (contracts: Contract[]) => {}

export interface ReleaseProps {
  release: Release
}

export const ReleaseCard = ({ release }: ReleaseProps) => {
  // TODO
  console.log(JSON.stringify(release, undefined, 2))
  const [title, subtitle] = release.parseTitle()
  if (release.tag.includes('award')) {
    const awards = release.awards!
    assert(awards.length === 1)
    const award = awards[0]
    return (
      <Card>
        <CardHeader>
          <Heading size="md" mb={2}>
            {title}
          </Heading>
          <Text fontSize="xl">{subtitle}</Text>
        </CardHeader>
        <CardBody>
          <Stack spacing="4">
            <SimpleGrid columns={2} spacing={2}>
              <Heading size="xs">Date</Heading>
              <Text fontSize="sm">{release.date.toDateString()}</Text>
              <Heading size="xs">Status</Heading>
              <Text fontSize="sm">{award.status}</Text>
              {release.contracts.length > 0 && (
                <Heading size="xs">Value</Heading>
              )}
              {release.contracts.length > 0 && (
                <Text fontSize="sm">{release.contractsValueFormatted()}</Text>
              )}
            </SimpleGrid>
            <Divider orientation="horizontal" />
            <Box>
              <Heading size="xs">Description</Heading>
              <Text pt="2" fontSize="sm">
                {award.description || release.tender!.description}
              </Text>
            </Box>
            <Accordion allowToggle allowMultiple>
              <AccordionItem>
                <AccordionButton>
                  <Heading size="sm">Buyer</Heading>
                  <AccordionIcon ml={1} />
                </AccordionButton>
                <AccordionPanel>
                  <OrganizationCard org={release.buyer!} />
                </AccordionPanel>
              </AccordionItem>
              <AccordionItem>
                <AccordionButton>
                  <Heading size="sm">Suppliers</Heading>
                  <AccordionIcon ml={1} />
                </AccordionButton>
                {award.suppliers!.map((sup, i) => (
                  <AccordionPanel key={i}>
                    <OrganizationCard org={sup} />
                  </AccordionPanel>
                ))}
              </AccordionItem>
            </Accordion>
          </Stack>
        </CardBody>
      </Card>
    )
  } else {
    const tender = release.tender!
    return (
      <Card>
        <CardHeader>
          <Heading size="md" mb={2}>
            {title}
          </Heading>
          <Text fontSize="xl">{subtitle}</Text>
        </CardHeader>
        <CardBody>
          <Stack spacing="4">
            <SimpleGrid columns={2} spacing={2}>
              <Heading size="xs">Date</Heading>
              <Text fontSize="sm">{release.date.toDateString()}</Text>
              <Heading size="xs">Status</Heading>
              <Text fontSize="sm">{tender.status}</Text>
            </SimpleGrid>
            <Divider orientation="horizontal" />
            <Box>
              <Heading size="xs">Description</Heading>
              <Text pt="2" fontSize="sm">
                {tender.description}
              </Text>
            </Box>
            <Accordion allowToggle>
              <AccordionItem>
                <AccordionButton>
                  <Heading size="sm">Buyer</Heading>
                  <AccordionIcon ml={1} />
                </AccordionButton>
                <AccordionPanel>
                  <OrganizationCard org={release.buyer} />
                </AccordionPanel>
              </AccordionItem>
            </Accordion>
          </Stack>
        </CardBody>
      </Card>
    )
  }
}
