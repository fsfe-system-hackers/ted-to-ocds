// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { Box, Flex } from '@chakra-ui/react'
import { Collapse } from '@chakra-ui/transition'
import { useEffect, useState } from 'react'
import { Scrollbars } from 'react-custom-scrollbars-2'

import { OCDS } from './OCDS'
import { TEDectiveNode } from '../../pages/index'

export interface SidebarProps {
  isOpen: boolean
  onToggle: any
  previewNode: TEDectiveNode | null
}

const Sidebar = (props: SidebarProps) => {
  const { isOpen, onToggle, previewNode } = props

  // TODO open sidebar when user clicks on node
  useEffect(() => {
    if (previewNode && !isOpen) {
      onToggle()
    }
  }, [previewNode])

  return (
    <Collapse in={isOpen} animateOpacity={false}>
      <Flex flexDir="column" color="black" bg="alt.100" width="md">
        <Scrollbars
          autoHide
          autoHeight={true}
          autoHeightMax="100vh"
          renderThumbVertical={({ style, ...props }) => (
            <Box
              style={{
                ...style,
                borderRadius: 0,
                // backgroundColor: highlightColor,
              }}
              {...props}
            />
          )}
        >
          {previewNode && (
            <OCDS
              {...{
                previewNode,
              }}
            />
          )}
        </Scrollbars>
      </Flex>
    </Collapse>
  )
}

export default Sidebar
