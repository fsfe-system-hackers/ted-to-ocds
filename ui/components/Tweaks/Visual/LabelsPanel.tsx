// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { ChevronDownIcon } from '@chakra-ui/icons'
import {
  Box,
  Button,
  Collapse,
  Flex,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Portal,
  StackDivider,
  VStack,
  Slider,
  SliderFilledTrack,
  SliderThumb,
  SliderTrack,
  Tooltip,
  Text,
} from '@chakra-ui/react'
import React from 'react'
import { initialVisuals } from '../../config'
import { SliderWithInfo } from '../SliderWithInfo'

export interface LabelsPanelProps {
  visuals: typeof initialVisuals
  setVisuals: any
}

export const LabelsPanel = (props: LabelsPanelProps) => {
  const { visuals, setVisuals } = props
  return (
    <VStack
      spacing={2}
      justifyContent="flex-start"
      divider={<StackDivider borderColor="gray.400" />}
      align="stretch"
      color="gray.800"
    >
      <SliderWithInfo
        label="Label font size"
        value={visuals.labelFontSize}
        min={5}
        max={20}
        step={0.5}
        onChange={(value) => setVisuals({ ...visuals, labelFontSize: value })}
      />
      <SliderWithInfo
        label="Max. label characters"
        value={visuals.labelLength}
        min={10}
        max={100}
        step={1}
        onChange={(value) => setVisuals({ ...visuals, labelLength: value })}
      />
      <SliderWithInfo
        label="Max. label line length"
        value={visuals.labelWordWrap}
        min={10}
        max={100}
        step={1}
        onChange={(value) => setVisuals({ ...visuals, labelWordWrap: value })}
      />
      <SliderWithInfo
        label="Space between label lines"
        value={visuals.labelLineSpace}
        min={0.2}
        max={3}
        step={0.1}
        onChange={(value) => setVisuals({ ...visuals, labelLineSpace: value })}
      />
    </VStack>
  )
}
