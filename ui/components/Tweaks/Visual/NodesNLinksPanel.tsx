// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { Box, StackDivider, VStack } from '@chakra-ui/react'
import { initialVisuals } from '../../config'
import { SliderWithInfo } from '../SliderWithInfo'

export interface NodesNLinksPanelProps {
  visuals: typeof initialVisuals
  setVisuals: any
}

export const NodesNLinksPanel = (props: NodesNLinksPanelProps) => {
  const { visuals, setVisuals } = props
  return (
    <VStack
      spacing={2}
      justifyContent="flex-start"
      divider={<StackDivider borderColor="gray.500" />}
      align="stretch"
      color="gray.800"
    >
      <Box>
        <SliderWithInfo
          label="Node size"
          value={visuals.nodeRel}
          onChange={(value) => setVisuals({ ...visuals, nodeRel: value })}
        />
        <SliderWithInfo
          label="Organization node size"
          value={visuals.orgNodeSize}
          step={1}
          onChange={(value) => setVisuals({ ...visuals, orgNodeSize: value })}
        />
        <SliderWithInfo
          label="Award node size multiplier"
          value={visuals.awardNodeSizeMult}
          min={0}
          max={2}
          onChange={(value) =>
            setVisuals({ ...visuals, awardNodeSizeMult: value })
          }
        />
        <SliderWithInfo
          label="Node zoom invariance"
          value={visuals.nodeZoomSize}
          min={0}
          max={2}
          infoText="How much the graph will try to keep the nodesize consistent across zoom scales. 0 is no consistency, node will always be their true size, 1 is linear, 2 is quadratic."
          onChange={(value) =>
            setVisuals((prev: typeof initialVisuals) => ({
              ...prev,
              nodeZoomSize: value,
            }))
          }
        />
        <SliderWithInfo
          label="Link width"
          value={visuals.linkWidth}
          onChange={(value) => setVisuals({ ...visuals, linkWidth: value })}
        />
        {/* <EnableSection
          label="Link arrows"
          value={visuals.arrows}
          onChange={() => setVisuals({ ...visuals, arrows: !visuals.arrows })}
        >
          <SliderWithInfo
            label="Arrow size"
            value={visuals.arrowsLength / 10}
            onChange={(value) => setVisuals({ ...visuals, arrowsLength: 10 * value })}
          />
          <SliderWithInfo
            label="Arrow Position"
            value={visuals.arrowsPos}
            min={0}
            max={1}
            step={0.01}
            onChange={(value) => setVisuals({ ...visuals, arrowsPos: value })}
          />
          <ColorMenu
            colorList={colorList}
            label="Arrow Color"
            key="arrow"
            setVisuals={setVisuals}
            value="arrowsColor"
            visValue={visuals.arrowsColor}
          />
        </EnableSection>
        <EnableSection
          label="Directional Particles"
          value={visuals.particles}
          onChange={() => setVisuals({ ...visuals, particles: !visuals.particles })}
        >
          <SliderWithInfo
            label="Particle Number"
            value={visuals.particlesNumber}
            max={5}
            step={1}
            onChange={(value) => setVisuals({ ...visuals, particlesNumber: value })}
          />
          <SliderWithInfo
            label="Particle Size"
            value={visuals.particlesWidth}
            onChange={(value) => setVisuals({ ...visuals, particlesWidth: value })}
          />
        </EnableSection> */}
      </Box>
    </VStack>
  )
}
