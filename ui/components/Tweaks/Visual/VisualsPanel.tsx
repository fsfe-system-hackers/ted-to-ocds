// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Flex,
  Text,
  VStack,
} from '@chakra-ui/react'

import { useCallback } from 'react'
import { initialVisuals } from '../../config'
import { LabelsPanel } from './LabelsPanel'
import { NodesNLinksPanel } from './NodesNLinksPanel'

export interface VisualsPanelProps {
  visuals: typeof initialVisuals
  setVisuals: any
  highlightColor: string
}

export const VisualsPanel = ({
  visuals,
  setVisuals,
  highlightColor,
}: VisualsPanelProps) => {
  const setVisualsCallback = useCallback((val) => setVisuals(val), [])
  return (
    <VStack justifyContent="flex-start" align="stretch">
      {/* <ThemeSelect /> */}
      {/* <GraphColorSelect {...{ coloring, setColoring }} /> */}
      {/* <AccordionItem>
          <AccordionButton>
            <Flex justifyContent="space-between" w="100%">
              <Text>Nodes & Links</Text>
              <AccordionIcon marginRight={2} />
            </Flex>
          </AccordionButton>
          <AccordionPanel>
            <NodesNLinksPanel
              visuals={visuals}
              setVisuals={setVisualsCallback}
            />
          </AccordionPanel>
        </AccordionItem> */}
      <Accordion allowToggle defaultIndex={[0]} paddingLeft={3}>
        <AccordionItem>
          <AccordionButton>
            <Flex justifyContent="space-between" w="100%">
              <Text>Nodes & Links</Text>
              <AccordionIcon marginRight={2} />
            </Flex>
          </AccordionButton>
          <AccordionPanel>
            <NodesNLinksPanel
              visuals={visuals}
              setVisuals={setVisualsCallback}
            />
          </AccordionPanel>
        </AccordionItem>
        <AccordionItem>
          <AccordionButton>
            <Flex justifyContent="space-between" w="100%">
              <Text>Labels</Text>
              <AccordionIcon marginRight={2} />
            </Flex>
          </AccordionButton>
          <AccordionPanel>
            <LabelsPanel visuals={visuals} setVisuals={setVisualsCallback} />
          </AccordionPanel>
        </AccordionItem>
      </Accordion>
    </VStack>
  )
}
