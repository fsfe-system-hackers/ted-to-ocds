// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

export const initialFilter = {
  dateRange: [NaN, NaN],
  minMaxYears: [NaN, NaN],
}

export const initialPhysics = {
  enabled: true,
  charge: -700,
  collision: true,
  collisionStrength: 20,
  // centering: true,
  // centeringStrength: 0.2,
  linkStrength: 0.3,
  // linkIts: 1,
  alphaDecay: 0.05,
  // alphaTarget: 0,
  alphaMin: 0,
  velocityDecay: 0.25,
  gravity: 0.1,
  gravityOn: true,
}

export const initialVisuals = {
  labels: 2,
  nodeRel: 3,
  labelFontSize: 8,
  labelLength: 40,
  labelWordWrap: 25,
  labelLineSpace: 1,
  labelTextColor: 'black',
  awardNodeSizeMult: 0.2,
  nodeZoomSize: 1.2,
  orgNodeSize: 2,
  linkWidth: 1.0,
  linkColor: 'gray.300',
  orgNodeColor: 'green.500',
  awardNodeColor: 'orange.500',
  tenderNodeColor: 'yellow.500',
  planningNodeColor: 'gray.500',
  highlightAnim: true,
  highlightFade: 0.8,
  highlightLinkSize: 1.4,
  highlightNodeSize: 2.2,
  highlightLabelFontSize: 1.4,
}
