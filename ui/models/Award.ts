// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { components } from '../api'

import { Organization } from './Organization'
import { Release } from './Release'

type AwardRead = components['schemas']['AwardRead']

export class Award {
  /**
   * Award ID
   * Format: uuid
   * @description The identifier for this award. It must be unique and must not change within the Open Contracting Process it is part of (defined by a single ocid). See the [identifier guidance](https://standard.open-contracting.org/1.1/en/schema/identifiers/) for further details.
   */
  id: string
  /**
   * Title
   * @description Award title
   */
  title: string
  /**
   * Description
   * @description Award description
   */
  description: string
  /**
   * Award status
   * @description The current status of the award, from the closed [awardStatus](https://standard.open-contracting.org/1.1/en/schema/codelists/#award-status) codelist.
   */
  status?: components['schemas']['AwardStatus']
  /**
   * Award date
   * Format: date-time
   * @description The date of the contract award. This is usually the date on which a decision to award was made.
   */
  date?: string
  /**
   * Items awarded
   * @description The goods and services awarded in this award, broken into line items wherever possible. Items should not be duplicated, but the quantity specified instead.
   */
  items?: components['schemas']['Item'][]
  /**
   * Contract period
   * @description The period for which the contract has been awarded.
   */
  contractPeriod?: components['schemas']['Period']
  /**
   * Documents
   * @description All documents and attachments related to the award, including any notices.
   */
  documents?: components['schemas']['Document'][]
  /**
   * Amendments
   * @description An award amendment is a formal change to the details of the award, and generally involves the publication of a new award notice/release. The rationale and a description of the changes made can be provided here.
   */
  amendments?: components['schemas']['Amendment'][]
  /**
   * Suppliers
   * @description The suppliers awarded this award. If different suppliers have been awarded different items or values, these should be split into separate award blocks.
   */
  suppliers: Organization[]

  constructor(award: AwardRead) {
    const { id, title, description, suppliers, ...rest } = award
    this.id = id!
    this.title = title!
    this.description = description!
    this.suppliers = suppliers!.map((org) => Release.maybeAddOrg(org))
    Object.assign(this, { ...rest })
  }
}
