// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { components } from '../api'
import assert from 'assert'

type OrganizationRead = components['schemas']['OrganizationRead']

export class Organization {
  /**
   * Common name
   * @description A common name for this organization or other participant in the contracting process. The identifier object provides a space for the formal legal name, and so this may either repeat that value, or may provide the common name by which this organization or entity is known. This field may also include details of the department or sub-unit involved in this contracting process.
   */
  name: string
  /**
   * Entity ID
   * Format: uuid
   * @description The ID used for cross-referencing to this party from other sections of the release. This field may be built with the following structure {identifier.scheme}-{identifier.id}(-{department-identifier}).
   */
  id: string
  /**
   * Primary identifier
   * @description The primary identifier for this organization or participant. Identifiers that uniquely pick out a legal entity should be preferred. Consult the [organization identifier guidance](https://standard.open-contracting.org/1.1/en/schema/identifiers/) for the preferred scheme and identifier to use.
   */
  identifier: components['schemas']['Identifier']
  /**
   * Additional identifiers
   * @description A list of additional / supplemental identifiers for the organization or participant, using the [organization identifier guidance](https://standard.open-contracting.org/1.1/en/schema/identifiers/). This can be used to provide an internally used identifier for this organization in addition to the primary legal entity identifier.
   */
  additionalIdentifiers?: components['schemas']['Identifier'][]
  /**
   * Address
   * @description An address. This may be the legally registered address of the organization, or may be a correspondence address for this particular contracting process.
   */
  address: components['schemas']['Address']
  /**
   * Contact point
   * @description Contact details that can be used for this party.
   */
  contactPoint?: components['schemas']['ContactPoint']
  /**
   * Party roles
   * @description The party's role(s) in the contracting process, using the open [partyRole](https://standard.open-contracting.org/1.1/en/schema/codelists/#party-role) codelist.
   */
  roles: string[]
  /**
   * Details
   * @description Additional classification information about parties can be provided using partyDetail extensions that define particular fields and classification schemes.
   */
  details?: Record<string, never>

  constructor(organization: OrganizationRead) {
    const { name, id, identifier, address, roles, ...rest } = organization
    this.name = name!
    this.id = id!
    this.identifier = identifier!
    this.address = address!
    this.roles = roles!
    Object.assign(this, { ...rest })
  }

  formatAddress() {
    const formatter = new Intl.DisplayNames('en', { type: 'region' })
    return [
      this.address.streetAddress!,
      `${this.address.postalCode!} ${this.address.locality!}`,
      formatter.of(this.address.countryCode!),
    ]
  }

  formatContactPoint() {
    return [
      `Email: ${this.contactPoint!.email!}`,
      `Tel.: ${this.contactPoint!.telephone!}`,
      `Fax.: ${this.contactPoint!.faxNumber!}`,
    ]
  }
}
