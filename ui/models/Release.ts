// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { components } from '../api'
import assert from 'assert'

import { Organization } from './Organization'
import { Award } from './Award'

type ReleaseRead = components['schemas']['ReleaseRead']
type OrganizationRead = components['schemas']['OrganizationRead']

export class Release {
  /**
   * Release ID
   * Format: uuid
   * @description An identifier for this particular release of information. A release identifier must be unique within the scope of its related contracting process (defined by a common ocid). A release identifier must not contain the # character.
   */
  id: string
  /**
   * Open Contracting ID
   * @description A globally unique identifier for this Open Contracting Process. Composed of an ocid prefix and an identifier for the contracting process. For more information see the [Open Contracting Identifier guidance](https://standard.open-contracting.org/1.1/en/schema/identifiers/)
   */
  ocid: string
  /**
   * Release Date
   * Format: date-time
   * @description The date on which the information contained in the release was first recorded in, or published by, any system.
   */
  date: Date
  /**
   * Tag
   * @description One or more values from the closed [releaseTag](https://standard.open-contracting.org/1.1/en/schema/codelists/#release-tag) codelist. Tags can be used to filter releases and to understand the kind of information that releases might contain.
   */
  tag: components['schemas']['TagEnum'][]
  /**
   * Initiation type
   * @description The type of initiation process used for this contract, from the closed [initiationType](https://standard.open-contracting.org/1.1/en/schema/codelists/#initiation-type) codelist.
   */
  initiationType: components['schemas']['InitiationType']
  /**
   * Release language
   * @description The default language of the data using either two-letter [ISO639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes), or extended [BCP47 language tags](http://www.w3.org/International/articles/language-tags/). The use of lowercase two-letter codes from [ISO639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) is recommended.
   * @default en
   */
  language: string
  /**
   * Parties
   * @description Information on the parties (organizations, economic operators and other participants) who are involved in the contracting process and their roles, e.g. buyer, procuring entity, supplier etc. Organization references elsewhere in the schema are used to refer back to this entries in this list.
   */
  parties: Organization[]
  /**
   * Buyer
   * @description A buyer is an entity whose budget will be used to pay for goods, works or services related to a contract. This may be different from the procuring entity who may be specified in the tender data.
   */
  buyer: Organization
  /**
   * Planning
   * @description Information from the planning phase of the contracting process. This includes information related to the process of deciding what to contract, when and how.
   */
  planning?: components['schemas']['PlanningRead']
  /**
   * Tender
   * @description The activities undertaken in order to enter into a contract.
   */
  tender?: components['schemas']['TenderRead']
  /**
   * Awards
   * @description Information from the award phase of the contracting process. There can be more than one award per contracting process e.g. because the contract is split among different providers, or because it is a standing offer.
   */
  awards: Award[]
  /**
   * Contracts
   * @description Information from the contract creation phase of the procurement process.
   */
  contracts: components['schemas']['Contract'][]
  /**
   * Related processes
   * @description The details of related processes: for example, if this process follows on from one or more other processes, represented under a separate open contracting identifier (ocid). This is commonly used to relate mini-competitions to their parent frameworks or individual tenders to a broader planning process.
   */
  relatedProcesses?: components['schemas']['RelatedProcess'][]

  static orgById: { [orgId: string]: Organization } = {}

  static maybeAddOrg(org: OrganizationRead) {
    const orgId = org.id!
    if (!Release.orgById[orgId]) {
      Release.orgById[orgId] = new Organization(org)
    }
    return Release.orgById[orgId]
  }

  constructor(release: ReleaseRead) {
    const {
      date,
      id,
      ocid,
      parties,
      buyer,
      tag,
      initiationType,
      awards,
      contracts,
      language,
      ...rest
    } = release
    this.id = id!
    this.ocid = ocid
    this.date = new Date(date)
    this.buyer = Release.maybeAddOrg(buyer!)
    this.parties = parties!.map((org) => Release.maybeAddOrg(org))
    this.tag = tag!
    this.awards = awards!.map((ar) => new Award(ar))
    this.contracts = contracts!
    this.initiationType = initiationType
    this.language = language!
    Object.assign(this, { ...rest })
  }

  parseTitle() {
    assert(this.tender && this.tender.title)
    const title = this.tender!.title!
    const index = title.indexOf('[')
    return [
      title.substring(0, index - 1),
      title.substring(index + 1, title.length - 1),
    ]
  }

  contractsValue() {
    return (
      this.contracts?.reduce((sum, contract) => {
        return sum + (contract.value?.amountEur ?? 0)
      }, 0) ?? 0
    )
  }

  contractsValueFormatted() {
    const value = this.contractsValue()
    const formatter = new Intl.NumberFormat('en', {
      style: 'currency',
      currency: 'EUR',
    })
    return formatter.format(value)
  }

  static parse(data: any) {
    const releaseData = data as ReleaseRead[]

    return releaseData.map((rel) => {
      return new Release(rel)
    })
  }
}
