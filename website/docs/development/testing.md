---
sidebar_position: 6
title: Testing
---

This page is about the setting up the environment for running tests and it explains how tests
are implemented in order to make it easier for developers to write their own tests for new functionality

<!--
SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

## Setting up the environment
In the backend [`./api`](https://git.fsfe.org/TEDective/tedective/src/branch/main/api), all tests are designed to work with
the actual instance of the database. Instead of adding the bulk data to the "real" database, for each test case will be a
clean testing database based on [schema](https://terminusdb.com/docs/schema-reference-guide/). To set it up we used environment
variables, that's why tests should be used withing the Docker container.

You can start the containers needed for testing by running:
`make dev.up.test`

This will start the two Docker containers: `terminusdb-test` and `tedective-api-test`. Both containers can be also accessed
manually under `0.0.0.0:6363` for TerminusDB and `0.0.0.0:9000` for the OpenAPI.

If everything gone well, you can run the tests with:
`make api.test`

## Understanding how tests are constructed
There are two tests files that covering most of backend functionality:

- `test_schema.py` - tests if database schema working as intended
- `test_api.py` - tests available if API endpoints are returning the data in correct format

### Setting up
Before each each test 