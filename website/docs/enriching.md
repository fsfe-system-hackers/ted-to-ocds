---
sidebar_position: 4
title: Enriching TED Data
---

<!--
SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# Interesting Data Sources

The following are interesting data sources that could be connected with TED Data
for public benefit. None of them are properly integrated yet.

## Corporations

- [OpenCorporates](https://www.opencorporates.com)
- [OpenSanctions](https://www.opensanctions.org)
- [ICIJ Offshore Leaks](https://offshoreleaks.icij.org)

## Geographic

- [Nuts2json](https://eurostat.github.io/Nuts2json)
- [eurostat-map.js](https://github.com/eurostat/eurostat-map.js)
