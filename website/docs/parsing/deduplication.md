---
sidebar_position: 2
title: Deduplication
---

This page describes how to make sure there are no duplicate entities in the TEDective 
dataset. This is especially relevant for organizations.

<!--
SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>

SPDX-License-Identifier: GPL-3.0-or-later
-->

[//]: # (# TODO Write documentation)
