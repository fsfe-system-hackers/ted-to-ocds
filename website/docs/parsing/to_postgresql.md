---
sidebar_position: 1
title: Parsing to TerminusDB
---

This page is about the initial parsing of XML files into the TerminusDB
database.

<!--
SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

## What a TED notice looks like

The notice below tells us that the University hospital in Gdańsk wants to
procure heart valves for PLN 8,600,000 which is roughly € 1.8 million.
Sometimes these XML files are several thousand lines long. In the example
below, I removed some translations of the term 'heart valve' to make this
more readable.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<TED_EXPORT xmlns:xlink="http://www.w3.org/1999/xlink"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xmlns="ted/R2.0.9.S02/publication"
            xmlns:n2016="ted/2016/nuts"
            xsi:schemaLocation="ted/R2.0.9.S02/publication TED_EXPORT.xsd"
            VERSION="R2.0.9.S02.E01"
            DOC_ID="000144-2018"
            EDITION="2018001">
   <TECHNICAL_SECTION>
      <RECEPTION_ID>17-745304-001</RECEPTION_ID>
      <DELETION_DATE>20180209</DELETION_DATE>
      <FORM_LG_LIST>PL </FORM_LG_LIST>
      <COMMENTS>From Convertor</COMMENTS>
   </TECHNICAL_SECTION>
   <LINKS_SECTION>
      <XML_SCHEMA_DEFINITION_LINK xlink:type="simple"
                                  xlink:href="http://ted.europa.eu"
                                  xlink:title="TED WEBSITE"/>
      <OFFICIAL_FORMS_LINK xlink:type="simple" xlink:href="http://ted.europa.eu"/>
      <FORMS_LABELS_LINK xlink:type="simple" xlink:href="http://ted.europa.eu"/>
      <ORIGINAL_CPV_LINK xlink:type="simple" xlink:href="http://ted.europa.eu"/>
      <ORIGINAL_NUTS_LINK xlink:type="simple" xlink:href="http://ted.europa.eu"/>
   </LINKS_SECTION>
   <CODED_DATA_SECTION>
      <REF_OJS>
         <COLL_OJ>S</COLL_OJ>
         <NO_OJ>1</NO_OJ>
         <DATE_PUB>20180103</DATE_PUB>
      </REF_OJS>
      <NOTICE_DATA>
         <NO_DOC_OJS>2018/S 001-000144</NO_DOC_OJS>
         <URI_LIST>
            <URI_DOC LG="PL">http://ted.europa.eu/udl?uri=TED:NOTICE:000144-2018:TEXT:PL:HTML</URI_DOC>
         </URI_LIST>
         <LG_ORIG>PL</LG_ORIG>
         <ISO_COUNTRY VALUE="PL"/>
         <IA_URL_GENERAL>www.uck.gda.pl</IA_URL_GENERAL>
         <IA_URL_ETENDERING>www.uck.gda.pl</IA_URL_ETENDERING>
         <ORIGINAL_CPV CODE="33182220">Cardiac valve</ORIGINAL_CPV>
         <n2016:PERFORMANCE_NUTS CODE="PL634">Gdański</n2016:PERFORMANCE_NUTS>
         <n2016:CA_CE_NUTS CODE="PL634">Gdański</n2016:CA_CE_NUTS>
         <VALUES>
            <VALUE TYPE="ESTIMATED_TOTAL" CURRENCY="PLN">8600000.00</VALUE>
         </VALUES>
      </NOTICE_DATA>
      <CODIF_DATA>
         <DS_DATE_DISPATCH>20171229</DS_DATE_DISPATCH>
         <DT_DATE_FOR_SUBMISSION>20180209 10:30</DT_DATE_FOR_SUBMISSION>
         <AA_AUTHORITY_TYPE CODE="8">Other</AA_AUTHORITY_TYPE>
         <TD_DOCUMENT_TYPE CODE="3">Contract notice</TD_DOCUMENT_TYPE>
         <NC_CONTRACT_NATURE CODE="2">Supplies</NC_CONTRACT_NATURE>
         <PR_PROC CODE="1">Open procedure</PR_PROC>
         <RP_REGULATION CODE="4">European Union</RP_REGULATION>
         <TY_TYPE_BID CODE="1">Submission for all lots</TY_TYPE_BID>
         <AC_AWARD_CRIT CODE="2">The most economic tender</AC_AWARD_CRIT>
         <MA_MAIN_ACTIVITIES CODE="H">Health</MA_MAIN_ACTIVITIES>
         <HEADING>01B02</HEADING>
         <INITIATOR>01</INITIATOR>
         <DIRECTIVE VALUE="2014/24/EU"/>
      </CODIF_DATA>
   </CODED_DATA_SECTION>
   <TRANSLATION_SECTION>
      <ML_TITLES>
         <ML_TI_DOC LG="EN">
            <TI_CY>Poland</TI_CY>
            <TI_TOWN>Gdansk</TI_TOWN>
            <TI_TEXT>
               <P>Cardiac valve</P>
            </TI_TEXT>
         </ML_TI_DOC>
      </ML_TITLES>
      <ML_AA_NAMES>
         <AA_NAME LG="PL">Uniwersyteckie Centrum Kliniczne</AA_NAME>
      </ML_AA_NAMES>
   </TRANSLATION_SECTION>
   <FORM_SECTION>
      <F02_2014 CATEGORY="ORIGINAL" FORM="F02" LG="PL">
         <CONTRACTING_BODY>
            <ADDRESS_CONTRACTING_BODY>
               <OFFICIALNAME>Uniwersyteckie Centrum Kliniczne</OFFICIALNAME>
               <ADDRESS>ul. Dębinki 7</ADDRESS>
               <TOWN>Gdańsk</TOWN>
               <POSTAL_CODE>80-952</POSTAL_CODE>
               <COUNTRY VALUE="PL"/>
               <CONTACT_POINT>Katarzyna Wójciga</CONTACT_POINT>
               <PHONE>+48 583492238</PHONE>
               <E_MAIL>kwojciga@uck.gda.pl</E_MAIL>
               <FAX>+48 583492074</FAX>
               <n2016:NUTS CODE="PL634"/>
               <URL_GENERAL>www.uck.gda.pl</URL_GENERAL>
            </ADDRESS_CONTRACTING_BODY>
            <DOCUMENT_FULL/>
            <URL_DOCUMENT>www.uck.gda.pl</URL_DOCUMENT>
            <ADDRESS_FURTHER_INFO>
               <OFFICIALNAME>Dział Zamówień Publicznych UCK ul. Dębinki 7 80-952 Gdańsk</OFFICIALNAME>
               <ADDRESS>ul. Dębinki 7</ADDRESS>
               <TOWN>Gdańsk</TOWN>
               <POSTAL_CODE>80-952</POSTAL_CODE>
               <COUNTRY VALUE="PL"/>
               <CONTACT_POINT>Katarzyna Wójciga</CONTACT_POINT>
               <E_MAIL>kwojciga@uck.gda.pl</E_MAIL>
               <n2016:NUTS CODE="PL634"/>
               <URL_GENERAL>www.uck.gda.pl</URL_GENERAL>
            </ADDRESS_FURTHER_INFO>
            <ADDRESS_PARTICIPATION>
               <OFFICIALNAME>Kancelaria Uniwersyteckiego Centrum Klinicznego ul. Dębinki 7 80-952 Gdańsk</OFFICIALNAME>
               <ADDRESS>ul.Dębinki 7 bud.nr 9, parter, pok. 100</ADDRESS>
               <TOWN>Gdańsk</TOWN>
               <POSTAL_CODE>80-952</POSTAL_CODE>
               <COUNTRY VALUE="PL"/>
               <CONTACT_POINT>Katarzyna Wójciga</CONTACT_POINT>
               <PHONE>+48 583492238</PHONE>
               <E_MAIL>kwojciga@uck.gda.pl</E_MAIL>
               <FAX>+48 583492074</FAX>
               <n2016:NUTS CODE="PL634"/>
               <URL_GENERAL>www.uck.gda.pl</URL_GENERAL>
            </ADDRESS_PARTICIPATION>
            <CA_TYPE_OTHER>Samodzielny Publiczny Zakład Opieki Zdrowotnej</CA_TYPE_OTHER>
            <CA_ACTIVITY VALUE="HEALTH"/>
         </CONTRACTING_BODY>
         <OBJECT_CONTRACT>
            <TITLE>
               <P>Dostawa zastawek typu Tavi dla UCK</P>
            </TITLE>
            <REFERENCE_NUMBER>190/PN/2017</REFERENCE_NUMBER>
            <CPV_MAIN>
               <CPV_CODE CODE="33182220"/>
            </CPV_MAIN>
            <TYPE_CONTRACT CTYPE="SUPPLIES"/>
            <SHORT_DESCR>
               <P>1. Przedmiotem zamówienia jest dostawa zastawek typu Tavi w ilości, asortymencie oraz wymaganiach bezwzględnych określonych w załączniku nr 3 do SIWZ.</P>
               <P>2. Niespełnienie chociażby jednego z wymagań bezwzględnych określonych w załączniku nr 3 do SIWZ spowoduje odrzucenie oferty.</P>
               <P>3. Zamawiający wymaga, aby zaoferowany asortymentspełniał wymagania dotyczące wyrobów medycznych zawarte w Ustawie z dnia 20 .5. 2010 roku o wyrobach medycznych (Dz.U. nr 107, poz. 679 z późn. zm.) oraz w wydanych na podstawie tejże ustawy, aktach wykonawczych.</P>
               <P>4. Dostawa jednorazowych i wielorazowych wyrobów medycznych będzie realizowana na podstawie zamówień częściowych. Max termin dostawy: 6 dni roboczych od daty złożenia zamówienia. Zamawiany asortyment Wykonawca zobowiązany będzie dostarczyć na własny koszt do Działu Zaopatrzenia Medycznego.</P>
            </SHORT_DESCR>
            <VAL_ESTIMATED_TOTAL CURRENCY="PLN">8600000.00</VAL_ESTIMATED_TOTAL>
            <LOT_DIVISION>
               <LOT_MAX_NUMBER>3</LOT_MAX_NUMBER>
            </LOT_DIVISION>
            <OBJECT_DESCR ITEM="1">
               <TITLE>
                  <P>Część 1</P>
               </TITLE>
               <LOT_NO>1</LOT_NO>
               <CPV_ADDITIONAL>
                  <CPV_CODE CODE="33182220"/>
               </CPV_ADDITIONAL>
               <n2016:NUTS CODE="PL634"/>
               <MAIN_SITE>
                  <P>Siedziba Zamawiającego.</P>
               </MAIN_SITE>
               <SHORT_DESCR>
                  <P>Szczegółowy opis zamieszczony został w załączniku nr 3 do SIWZ.</P>
               </SHORT_DESCR>
               <AC_QUALITY>
                  <AC_CRITERION>Termin dostawy</AC_CRITERION>
                  <AC_WEIGHTING>40</AC_WEIGHTING>
               </AC_QUALITY>
               <AC_PRICE>
                  <AC_WEIGHTING>60</AC_WEIGHTING>
               </AC_PRICE>
               <VAL_OBJECT CURRENCY="PLN">5120000.00</VAL_OBJECT>
               <DURATION TYPE="MONTH">12</DURATION>
               <NO_RENEWAL/>
               <NO_ACCEPTED_VARIANTS/>
               <NO_OPTIONS/>
               <NO_EU_PROGR_RELATED/>
            </OBJECT_DESCR>
            <OBJECT_DESCR ITEM="2">
               <TITLE>
                  <P>Część 2</P>
               </TITLE>
               <LOT_NO>2</LOT_NO>
               <CPV_ADDITIONAL>
                  <CPV_CODE CODE="33182220"/>
               </CPV_ADDITIONAL>
               <n2016:NUTS CODE="PL634"/>
               <MAIN_SITE>
                  <P>Siedziba Zamawiającego.</P>
               </MAIN_SITE>
               <SHORT_DESCR>
                  <P>Szczegółowy opis zamieszczony został w załączniku nr 3 do SIWZ.</P>
               </SHORT_DESCR>
               <AC_QUALITY>
                  <AC_CRITERION>Termin dostawy</AC_CRITERION>
                  <AC_WEIGHTING>40</AC_WEIGHTING>
               </AC_QUALITY>
               <AC_PRICE>
                  <AC_WEIGHTING>60</AC_WEIGHTING>
               </AC_PRICE>
               <VAL_OBJECT CURRENCY="PLN">2160000.00</VAL_OBJECT>
               <DURATION TYPE="MONTH">12</DURATION>
               <NO_RENEWAL/>
               <NO_ACCEPTED_VARIANTS/>
               <NO_OPTIONS/>
               <NO_EU_PROGR_RELATED/>
            </OBJECT_DESCR>
            <OBJECT_DESCR ITEM="3">
               <TITLE>
                  <P>Część 3</P>
               </TITLE>
               <LOT_NO>3</LOT_NO>
               <CPV_ADDITIONAL>
                  <CPV_CODE CODE="33182220"/>
               </CPV_ADDITIONAL>
               <n2016:NUTS CODE="PL634"/>
               <MAIN_SITE>
                  <P>Siedziba Zamawiającego.</P>
               </MAIN_SITE>
               <SHORT_DESCR>
                  <P>Szczegółowy opis zamieszczony został w załączniku nr 3 do SIWZ.</P>
               </SHORT_DESCR>
               <AC_QUALITY>
                  <AC_CRITERION>Termin dostawy</AC_CRITERION>
                  <AC_WEIGHTING>40</AC_WEIGHTING>
               </AC_QUALITY>
               <AC_PRICE>
                  <AC_WEIGHTING>60</AC_WEIGHTING>
               </AC_PRICE>
               <VAL_OBJECT CURRENCY="PLN">1320000.00</VAL_OBJECT>
               <DURATION TYPE="MONTH">12</DURATION>
               <NO_RENEWAL/>
               <NO_ACCEPTED_VARIANTS/>
               <NO_OPTIONS/>
               <NO_EU_PROGR_RELATED/>
            </OBJECT_DESCR>
         </OBJECT_CONTRACT>
         <LEFTI>
            <SUITABILITY>
               <P>Zamawiający nie wyznacza szczegółowego warunku w tym zakresie.</P>
            </SUITABILITY>
            <ECONOMIC_FINANCIAL_INFO>
               <P>Zamawiający nie wyznacza szczegółowego warunku w tym zakresie.</P>
            </ECONOMIC_FINANCIAL_INFO>
            <TECHNICAL_PROFESSIONAL_INFO>
               <P>Warunek ten zostanie spełniony jeżeli Wykonawcawykaże, że w okresie ostatnich 3 lat przed upływem terminu składania ofert, a jeżeli okres prowadzenia działalności jest krótszy to w tym okresie, wykonał lub wykonuje co najmniej dwie dostawywyrobów medycznych o wartości brutto każdej z nich nie mniejszejniż:</P>
               <P>Część 1 – 1 024 000,00 PLN.</P>
               <P>Część 2 – 432 000,00 PLN.</P>
               <P>Część 3 – 264 000,00 PLN.</P>
               <P>W przypadku złożenia ofert na kilka części, wartość dostaw stanowi sumę dostaw brutto ustalonych dla poszczególnych części.</P>
               <P>Warunek zostanie wstępnie zweryfikowany na podstawie wypełnionej Części IV: Kryteria kwalifikacji poprzez złożenie ogólnego oświadczenia dotyczącego wszystkich kryteriów kwalifikacji w pkt. α (alfa) w JEDZ.</P>
            </TECHNICAL_PROFESSIONAL_INFO>
            <PERFORMANCE_CONDITIONS>
               <P>Zgodnie z projektem umowy stanowiącym zał. nr 4 do SIWZ.</P>
            </PERFORMANCE_CONDITIONS>
         </LEFTI>
         <PROCEDURE>
            <PT_OPEN/>
            <NO_CONTRACT_COVERED_GPA/>
            <DATE_RECEIPT_TENDERS>2018-02-09</DATE_RECEIPT_TENDERS>
            <TIME_RECEIPT_TENDERS>10:30</TIME_RECEIPT_TENDERS>
            <LANGUAGES>
               <LANGUAGE VALUE="PL"/>
            </LANGUAGES>
            <OPENING_CONDITION>
               <DATE_OPENING_TENDERS>2018-02-09</DATE_OPENING_TENDERS>
               <TIME_OPENING_TENDERS>11:00</TIME_OPENING_TENDERS>
               <PLACE>
                  <P>Uniwersyteckie Centrum Kliniczne Dział Zamówień Publicznych ul. Dębinki 7, 80-952 Gdańsk, budynek nr 9,parter.</P>
               </PLACE>
            </OPENING_CONDITION>
         </PROCEDURE>
         <COMPLEMENTARY_INFO>
            <NO_RECURRENT_PROCUREMENT/>
            <INFO_ADD>
               <P>1. Zamawiający nie przewiduje udzielaniazamówień o których mowa w art. 67 ust 1 pkt.7 ustawy Pzp.</P>
               <P>2. Zamawiający nie dopuszcza składania ofert wariantowych.</P>
               <P>3. Zamawiający dopuszcza składanie ofert częściowych.</P>
               <P>4. Zamawiający dopuszcza zmianę umowy zgodnie z załącznikiem 4do SIWZ.</P>
               <P>5. Wprzypadku, gdy wartości podane przez Wykonawców na oświadczeniach i dokumentach,</P>
               <P>O których mowa w pkt VII SIWZ, podane będą w walucie innej niż PLN, Zamawiający przeliczy te wartości na PLN przyjmując średni kurs NBP danej waluty na dzień wszczęcia postępowania.</P>
               <P>6. Wszelkie nieuregulowane w niniejszym SIWZ czynności, uprawnienia, obowiązki Wykonawców</P>
               <P>I Zamawiającego, których ustawa nie nakazała zawierać Zamawiającemu w SIWZ, a które mogą przyczynić się do właściwego przebiegu postępowania, reguluje ustawa PZP.</P>
               <P>7. Zamawiający przewiduje dokonanie zmian umowy w toku jej realizacji w przypadku zaistnienia okoliczności, o których mowa w art. 144. Dz.U. z 2017 poz. 1579 ze zm.</P>
            </INFO_ADD>
            <ADDRESS_REVIEW_BODY>
               <OFFICIALNAME>Prezes Krajowej Izby Odwoławczej</OFFICIALNAME>
               <ADDRESS>ul.Postępu 17 A</ADDRESS>
               <TOWN>Warszawa</TOWN>
               <POSTAL_CODE>02-676</POSTAL_CODE>
               <COUNTRY VALUE="PL"/>
            </ADDRESS_REVIEW_BODY>
            <REVIEW_PROCEDURE>
               <P>Zasady wnoszenia środków ochrony prawnej w niniejszym postępowaniu regulują przepisy Działu VI ustawy zdnia 29.1.2004 roku Prawo zamówień Publicznych (Dz.U. z 2017 poz. 1579 ze zm.).</P>
            </REVIEW_PROCEDURE>
            <ADDRESS_REVIEW_INFO>
               <OFFICIALNAME>Krajowa Izba Odwoławcza</OFFICIALNAME>
               <ADDRESS>ul. Postępu 17 A</ADDRESS>
               <TOWN>Warszawa</TOWN>
               <POSTAL_CODE>02-676</POSTAL_CODE>
               <COUNTRY VALUE="PL"/>
            </ADDRESS_REVIEW_INFO>
            <DATE_DISPATCH_NOTICE>2017-12-29</DATE_DISPATCH_NOTICE>
         </COMPLEMENTARY_INFO>
      </F02_2014>
   </FORM_SECTION>
</TED_EXPORT>
```

## Parsing to OCDS

The first step is parsing this to OCDS.

