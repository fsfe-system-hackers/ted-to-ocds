---
sidebar_position: 1
title: Quickstart
---

<!--
SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

## Prerequisites

You need to have Docker and the `docker compose` CLI plugin ([Compose
V2](https://docs.docker.com/compose/reference/)) installed. Another
dependency is `make`. Development takes place at the [FSFE's Gitea
instance](https://git.fsfe.org/TEDective/tedective/src/branch/main/docs).
This means that you will need to have an FSFE account in order to be able
to create issues or pull requests there. In order for this to happen
simply register via [this link](https://my.fsfe.org/register/linus) and
wait until we have confirmed your application.

## Development

To start developing on TEDective locally, clone the repository (this will only
work if you have an FSFE account):

```shell
git clone git@git.fsfe.org:TEDective/tedective.git
```

If you don't have one yet, you can also do:

```shell
git clone https://git.fsfe.org/TEDective/tedective.git
```

and then simply run to bring up a development environment:

```shell
make dev.install
```

This will bring up the following service(s) that you can access with a web browser:

- [`localhost:9000`](http://localhost:9000) Swagger UI for FastAPI project
  in `./api`

It will also start

- a TerminusDB database on port `6363`

For more information's about development, head on to [development](/docs/category/development) (WIP).

Now you are ready to start [parsing](/docs/category/parsing-ted-data) (WIP),
[enriching](/docs/enriching) (WIP) and [analysing](/docs/analysis) (WIP) TED data.

## Project structure

- [`./api`](https://git.fsfe.org/TEDective/tedective/src/branch/main/api)
  is where the Python code for the TEDective API lives.
- [`./api/in`](https://git.fsfe.org/TEDective/tedective/src/branch/main/api/in)
  is where the input data goes, primarily the TED XML files.
- [`./api/graph`](https://git.fsfe.org/TEDective/tedective/src/branch/main/api/out)
  is where the output of the parsing process goes, e.g. OCDS-compliant JSON
  files.
- [`./ui`](https://git.fsfe.org/TEDective/tedective/src/branch/main/ui)
- is where the frontend of the application lives. 
- [`./website`](https://git.fsfe.org/TEDective/tedective/src/branch/main/website)
  is the directory from which this website is created.
