// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: CC0-1.0

// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github')
const darkCodeTheme = require('prism-react-renderer/themes/dracula')

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'TEDective',
  tagline: 'TEDective makes European public procurement data explorable for non-experts',
  url: 'https://tedective.org',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  plugins: [require.resolve('docusaurus-lunr-search')],

  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://git.fsfe.org/TEDective/tedective/src/branch/main/website',
        },
        blog: {
          showReadingTime: true,
          // Remove this to remove the "edit this page" links.
          editUrl:
            'https://git.fsfe.org/TEDective/tedective/src/branch/main/website',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],

  themeConfig:
  /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      metadata: [{ name: 'keywords', content: 'procurement, eu, free software' },
      ],
      navbar: {
        title: 'TEDective',
        logo: {
          alt: 'TEDective',
          src: 'img/logo.png',
        },
        items: [
          {
            type: 'doc',
            docId: 'quickstart',
            position: 'left',
            label: 'Docs',
          },
          { to: '/blog', label: 'Blog', position: 'left' },
          {
            href: 'https://git.fsfe.org/TEDective/tedective',
            label: 'Git',
            position: 'right',
          },
        ],
      },
      footer: {
        style: 'light',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Quickstart',
                to: '/docs/quickstart',
              },
            ],
          },
          {
            title: 'More',
            items: [
              {
                label: 'TEDective Blog',
                to: '/blog',
              },
              {
                label: 'FSFE Website',
                href: 'https://fsfe.org',
              },
              {
                label: 'Git',
                href: 'https://git.fsfe.org/TEDective/tedective',
              },
            ],
          },
          {
            title: 'Community',
            items: [
              {
                label: 'Discourse',
                href: 'https://community.fsfe.org',
              },
              {
                label: 'Mastodon',
                href: 'https://mastodon.social/@fsfe',
              },
              {
                label: 'Twitter',
                href: 'https://twitter.com/fsfe',
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Free Software Foundation Europe.`,
      },
      colorMode: {
        defaultMode: 'light',
        disableSwitch: true,
      },
      // Disabled for now
      // prism: {
      //   theme: lightCodeTheme,
      //   darkTheme: darkCodeTheme,
      // },
    }),
}

module.exports = config
