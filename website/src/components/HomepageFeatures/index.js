// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: CC0-1.0

import React from "react";
import clsx from "clsx";
import styles from "./styles.module.css";

const FeatureList = [
  {
    title: "TEDective reduces complexity",
    Svg: require("@site/static/img/undraw_data.svg").default,
    description: (
      <>
        TEDective allows you to make some sense out of the thousands of XML
        files published daily by the EU's{" "}
        <a href="https://ted.europa.eu/TED/browse/browseByMap.do">
          Tenders Electronic Daily
        </a>{" "}
        (TED).
      </>
    ),
  },
  {
    title: "TEDective is free software",
    Svg: require("@site/static/img/undraw_work_together.svg").default,
    description: (
      <>
        TEDective is a <a href="https://reuse.software">REUSE-compliant</a>{" "}
        project developed and maintained under a free software license, mostly{" "}
        <code>GPL-3.0-or-later</code>.
      </>
    ),
  },
  {
    title: "TEDective is interoperable",
    Svg: require("@site/static/img/undraw_team_collaboration.svg").default,
    description: (
      <>
        All data parsed by TEDective will be freely accessible via a powerful
        API and all published data will conform to the{" "}
        <a href="https://standard.open-contracting.org/latest/en/">
          Open Contracting Data Standard
        </a>{" "}
        (OCDS)
      </>
    ),
  },
];

function Feature({ Svg, title, description }) {
  return (
    <div className={clsx("col col--4")}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
