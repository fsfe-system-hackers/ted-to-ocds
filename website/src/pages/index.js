// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: CC0-1.0

import React from 'react'
import clsx from 'clsx'
import Link from '@docusaurus/Link'
import useDocusaurusContext from '@docusaurus/useDocusaurusContext'
import Layout from '@theme/Layout'
import HomepageFeatures from '@site/src/components/HomepageFeatures'

import styles from './index.module.css'

function HomepageHeader() {
  const { siteConfig } = useDocusaurusContext();
  return (
    <header className={clsx("hero hero--primary", styles.heroBanner)}>
      <div className="container">
        <h1 className="hero__title">Explore European public procurement!</h1>
        <p className="hero__subtitle">{siteConfig.tagline}</p>
        <div className={styles.buttons}>
          <div>
            <Link
              className="margin--sm button button--secondary button--lg"
              to="/docs/quickstart"
            >
              Documentation
            </Link>
          </div>
          // <div>
          //   <Link
          //     className="margin--sm button button--primary button--lg"
          //     to="https://app.tedective.org"
          //   >
          //     Explore UI →
          //   </Link>
          // </div>
          // <div>
          //   <Link
          //     className="margin--sm button button--primary button--lg"
          //     to="https://api.tedective.org/latest/docs"
          //   >
          //     Check out API →
          //   </Link>
          // </div>
        </div>
      </div>
    </header>
  );
}

export default function Home() {
  const { siteConfig } = useDocusaurusContext();
  return (
    <Layout
      title="Explore European Public Procurement"
      description="Making European public procurement data explorable for non-experts"
    >
      <HomepageHeader />
      <main>
        <HomepageFeatures />
      </main>
    </Layout>
  );
}
